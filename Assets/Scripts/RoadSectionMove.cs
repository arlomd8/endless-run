using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSectionMove : MonoBehaviour
{
    public Vector3 movementAxis;

    public bool isSectionTwo;
    private void Start()
    {
        //SpawnedItemChecker();
        //if (!isSectionTwo)
        //{
        //    GameManager.instance.OnRoadSpawn += SpawnedItemChecker;
        //}
        //else
        //{
        //    GameManager.instance.OnRoadTwoSpawn += SpawnedItemChecker;

        //}
    }

    private void Update()
    {
        if (GameManager.instance.isPlaying)
        {
            transform.position += movementAxis * Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Destroyer"))
        {
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }

    
}
