using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerHandler : MonoBehaviour
{
    float inputX;
    float inputY;

    Vector3 movement;
    
    [SerializeField] private const float RIGHT_X = 0.84f;
    [SerializeField]private const float LEFT_X = -0.84F;
    private const float MAX_X = 8.2f;
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed;
    [SerializeField] float jumpForce;

    [SerializeField] private int currentHealth;
    public int maxHealth;
    public UnityAction<int, int> OnTakeDamage;
    public UnityAction<int> OnTakeItem;

    private void Start()
    {
        currentHealth = maxHealth;
        OnTakeDamage += GameManager.instance.PlayerTakeDamage;
        OnTakeItem += GameManager.instance.PlayerTakeItem;

        UIManager.instance?.SetInitialLife();
        //OnTakeDamage?.Invoke(currentHealth, maxHealth);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Trigger") && GameManager.instance.isPlaying)
        {
            GameManager.instance.SpawnSection();
        }

        else if (other.gameObject.CompareTag("Item") && GameManager.instance.isPlaying)
        {
            FindObjectOfType<AudioManager>().Play("Daun");
            TakeItem(5);
            //Destroy(other.gameObject);
            other.transform.parent.gameObject.SetActive(false);
        }

        else if (other.gameObject.CompareTag("Botol") && GameManager.instance.isPlaying)
        {
            FindObjectOfType<AudioManager>().Play("Botol");
            TakeItem(10);
            UIManager.instance.ShowUSPText();
            //Destroy(other.gameObject);
            other.transform.parent.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("Enemy") && GameManager.instance.isPlaying)
        {
            FindObjectOfType<AudioManager>().Play("Enemy");
            TakeDamage();
            //Destroy(other.gameObject);
            other.transform.parent.gameObject.SetActive(false);
        }
    }

    //USING KEYBOARD
    private void Update()
    {
        if(!GameManager.instance.isPlaying)
        {
            return;
        }

        if (!GameManager.instance.usingKinect)
        {
            inputX = Input.GetAxis("Horizontal");
            movement = new Vector3(speed * inputX, transform.position.y, transform.position.z);
        }
        
    }


    private void FixedUpdate()
    {
        if (!GameManager.instance.usingKinect)
        {
            rb.velocity = movement;
        }
        else
        {
            if(movement.x > -0.9f && movement.x < 0.9f)
            {
                rb.position = movement;
            }
        }

        
    }



    public void TakeDamage()
    {
        if (currentHealth > 0)
        {
            currentHealth -= 1;
        }
        else
        {
            currentHealth = 0;
        }

        OnTakeDamage?.Invoke(currentHealth, maxHealth);
    }

    public void TakeItem(int s)
    {
        OnTakeItem?.Invoke(s);
    }

    public void UpdatePositionByKinect(Transform tr)
    {
        //HARUS 0.9f hasil dari local position kinect * faktor pengali
        if(!GameManager.instance.isPlaying) return;

        if(!GameManager.instance.usingKinect) return;


        movement = new Vector3(tr.position.x * 0.1f, transform.position.y, transform.position.z);
        //movement = new Vector3(tr.position.x * 0.1f , 0, 0);
        //transform.position = movement;

            
        
    }
}
