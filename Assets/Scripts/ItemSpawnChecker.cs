using System.Collections;
using System.Collections.Generic;
//using UnityEditor.ShaderKeywordFilter;
using UnityEngine;

public class ItemSpawnChecker : MonoBehaviour
{
    public ItemSpawner leftSpawner;
    public ItemSpawner midSpawner;
    public ItemSpawner rightSpawner;

    public RoadSectionMove roadMover;

    private void Start()
    {
        SpawnedItemChecker();
        if (!roadMover.isSectionTwo)
        {
            GameManager.instance.OnRoadSpawn += SpawnedItemChecker;
        }
        else
        {
            GameManager.instance.OnRoadTwoSpawn += SpawnedItemChecker;

        }
    }
    public void SpawnedItemChecker()
    {
        int leftNumber = Random.Range(0, 3);
        int midNumber = Random.Range(0, 3);
        int rightNumber = Random.Range(0, 3);
        int bottlePos = Random.Range(0,3);

        //print($"{leftNumber}, {midNumber}, {rightNumber}");

        // ALL OBSTACLE 
        if (leftNumber == 2 && midNumber == 2 && rightNumber == 2)
        {
            leftNumber = 0;
            midNumber = 0;
            rightNumber = 0;
        }

        // ALL DAUN 
        if(leftNumber == 1 || midNumber == 1 || rightNumber == 1) 
        {
            GameManager.instance.botolCounter += 1;
        }

        
        if(GameManager.instance.botolCounter == 5)
        {
            GameManager.instance.botolCounter = 0;

            //left
            if(bottlePos == 0)
                leftNumber = 3;
            
            //mid
            else if(bottlePos == 1)
                midNumber = 3;

            //right   
            else if(bottlePos == 2)
                rightNumber = 3;
        }

        leftSpawner.SpawnItem(leftNumber);
        midSpawner.SpawnItem(midNumber);
        rightSpawner.SpawnItem(rightNumber);
    }
}
