using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public Transform parentTransform;
    public GameObject superParent;
    
    public List<GameObject> Item;
    public GameObject itemObject;
    public GameObject enemyObject;
    public EnemyRandom enemyRandom;

    public GameObject botolObject;
    

    public void SpawnItem(int i)
    {
        itemObject.SetActive(false);
        enemyObject.SetActive(false);
        botolObject.SetActive(false);
        enemyRandom.RandomizeEnemyAsset();

        //int i = Random.Range(0, 2);

        if (i == 0)
        {
            itemObject.SetActive(false);
            enemyObject.SetActive(false);
            botolObject.SetActive(false);
        }
        else if(i == 1) // item
        {
            itemObject.SetActive(true);
            enemyObject.SetActive(false);
            botolObject.SetActive(false);
        }
        else if(i == 2) // enemy
        {
            itemObject.SetActive(false);
            enemyObject.SetActive(true);
            botolObject.SetActive(false);
        }
        else if(i == 3) // botol
        {
            itemObject.SetActive(false);
            enemyObject.SetActive(false);
            botolObject.SetActive(true);
        }
    }
}
