using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using DG;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [Header("GAMEPLAY")]
    public TextMeshProUGUI lifeText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI uspText;

    [Header("GAMEOVER")]
    public GameObject gameoverPanel;
    public TextMeshProUGUI gameoverScoreText;
    public TextMeshProUGUI gameoverTimerText;

    [Header("GAMESTART")]
    public GameObject gamestartPanel;
    public TextMeshProUGUI gamestartTimerText;


    [Header("ALERT")]
    public Image redAlert;
    public Image greenAlert;

    [Header("OTHERS")]
    public GameObject[] lifes;
    public string[] jargons;
    public GameObject[] uspBoards;
    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        GameManager.instance.OnTimeTicking += SetTimerGUI;
    }
    public void SetHealthGUI(int healthAmount, int maxHealth)
    {
        //lifeText.text = $"{healthAmount}/{maxHealth}";
        lifes[healthAmount].SetActive(false);
    }

    public void SetScoreGUI(int score)
    {
        scoreText.text = $"{score}";
        gameoverScoreText.text = $"{score}";
    }

    public void SetTimerGUI(float _time, float _maxTime)
    {
        timerText.text = _time.ToString("0") + "s";
    }

    public void ShowGameOverPanel()
    {
        if (!gameoverPanel.activeInHierarchy)
        {
            FindObjectOfType<AudioManager>().Play("GameOver");
            gameoverPanel.SetActive(true);
        }
        
    }



    public void ShowUSPText()
    {
        //uspText.text = jargons[Random.Range(0, jargons.Length)];
        int r = Random.Range(0, uspBoards.Length);

        FindObjectOfType<AudioManager>().Play($"usp{r + 1}");

        uspBoards[r].GetComponent<Image>().color = new Color32(255,255,255,0);
        uspBoards[r].GetComponent<RectTransform>().localPosition = new Vector3(0, 110f, 0);

        uspBoards[r].SetActive(true);
        uspBoards[r].GetComponent<Image>().DOFade(1, 1);
        uspBoards[r].GetComponent<RectTransform>().DOAnchorPosY(208f, 3f).OnComplete(() => {
            uspBoards[r].SetActive(false);
            uspBoards[r].GetComponent<Image>().color = new Color32(255, 255, 255, 0);
            uspBoards[r].GetComponent<RectTransform>().localPosition = new Vector3(0, 110f, 0);
        });
       
    }

    public void SetInitialLife()
    {
        foreach (GameObject life in lifes)
        {
            life.SetActive(true);
        }
    }


    private void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.C))
        {
            ShowUSPText();
        }
        #endif

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                //GameManager.instance.ChangeKinectMode();
            }
        }
        
    }


    public void SetGameStart(int timer)
    {
        if(timer <= 0)
        {
            gamestartTimerText.text = $"MULAI!";
        }
        else
        {
            gamestartTimerText.text = $"{timer}";
        }
    }

    public void HideGameStart()
    {
        gamestartPanel.SetActive(false);
    }

    public void SetGameOver(int timer)
    {
        if(timer < 0)
        {
            gameoverTimerText.text = $"0s";
        }
        else
        {
            gameoverTimerText.text = $"{timer}s";
        }
        
    }
    
    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void ShowRedAlert()
    {   
        redAlert.color = new Color32(255,0,0,0);
        redAlert.DOFade(0.3f, 0.2f)
        .SetLoops(3, LoopType.Yoyo)
        .SetEase(Ease.Linear).OnComplete(() => 
        {
            redAlert.DOFade(0f, 0.2f);
        });
    }

    public void ShowGreenAlert()
    {
        greenAlert.DOFade(0.3f, 0.2f)
        .SetLoops(3, LoopType.Yoyo)
        .SetEase(Ease.Linear).OnComplete(() => 
        {
            greenAlert.DOFade(0f, 0.2f);
        });
    }
}

