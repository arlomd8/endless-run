using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UserManager : MonoBehaviour
{
    public static UserManager instance;

    [Header("User")]
    public string username;
    public int score;

    [Header("Leaderboard")]
    public PlayerList playerList = new();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);

    }
    
    public void SetUsernameCard(string _username)
    {
        username = _username;
    }

    public void SetResultScore(int _score)
    {
        score = _score;
    }

    public void ResetUser()
    {
        username = "";
        score = 0;
    }

    public void SubmitToLeaderBoard(Player p)
    {   
        if(playerList.players == null)
        {
            playerList.players = new List<Player>();
        }
        playerList.players.Add(p);
        
        SortPlayers();
    }

    public void SortPlayers()
    {
        List<Player> sortedPlayers = playerList.players.OrderByDescending(p => p.score).ToList();
        
        playerList.players = new List<Player>(sortedPlayers);

        // if(playerList.players.Count > 10)
        // {
        //     playerList.players.Capacity = 10;
        // }

        SaveSystem.instance.SaveData(playerList, "Leaderboard");
    }
    
}
