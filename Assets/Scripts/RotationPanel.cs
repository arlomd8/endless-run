using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPanel : MonoBehaviour
{
    [Header("MenuSection")]
    public GameObject menuPanel;
    public GameObject tutorialPanel;
    public GameObject ulat;

    [Header("LeaderboardSection")]
    public GameObject leaderBoardPanel;

    [Header("VideoSection")]
    public GameObject videoPanel;
    public GameObject[] videoSection;

    public Coroutine runningCoroutine;

    private void Start()
    {
        if (runningCoroutine != null)
        {
            StopCoroutine(runningCoroutine);
        }
        runningCoroutine = StartCoroutine(nameof(PanelRotation));
    }


    public void RotationPanelClick()
    {
        if (runningCoroutine != null)
        {
            StopCoroutine(runningCoroutine);
        }

        tutorialPanel.SetActive(false);
        if (menuPanel.activeInHierarchy)
        {   
            ulat.SetActive(false);
            menuPanel.SetActive(false);
            leaderBoardPanel.SetActive(true);
            videoPanel.SetActive(false);
        }
        else if(leaderBoardPanel.activeInHierarchy)
        {
            ulat.SetActive(false);
            menuPanel.SetActive(false);
            leaderBoardPanel.SetActive(false);
            videoPanel.SetActive(true);
            VideoManager.instance.StartDefaultVideo();
        }
        else
        {
            ulat.SetActive(true);
            menuPanel.SetActive(true);
            leaderBoardPanel.SetActive(false);
            videoPanel.SetActive(false);
            VideoManager.instance.StopDefaultVideo();
        }

        runningCoroutine = StartCoroutine(nameof(PanelRotation));

    }

    public IEnumerator PanelRotation()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        yield return new WaitForSeconds(20f);

        tutorialPanel.SetActive(false);
        if (menuPanel.activeInHierarchy)
        {   
            ulat.SetActive(false);
            menuPanel.SetActive(false);
            leaderBoardPanel.SetActive(true);
            videoPanel.SetActive(false);
        }
        else if(leaderBoardPanel.activeInHierarchy)
        {
            ulat.SetActive(false);
            menuPanel.SetActive(false);
            leaderBoardPanel.SetActive(false);
            videoPanel.SetActive(true);
            VideoManager.instance.StartDefaultVideo();
        }
        else
        {
            ulat.SetActive(true);
            menuPanel.SetActive(true);
            leaderBoardPanel.SetActive(false);
            videoPanel.SetActive(false);
            VideoManager.instance.StopDefaultVideo();
        }


        if (runningCoroutine != null)
        {
            StopCoroutine(runningCoroutine);
        }
        runningCoroutine = StartCoroutine(nameof(PanelRotation));
    }

    public void StopPanelRotation()
    {
        if (runningCoroutine != null)
        {
            StopCoroutine(runningCoroutine);
        }
    }

    public void StartPanelRotation()
    {
        runningCoroutine = StartCoroutine(nameof(PanelRotation));
    }
}
