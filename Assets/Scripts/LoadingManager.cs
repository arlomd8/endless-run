using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour
{
    public static LoadingManager instance;
    public GameObject loadingPanel;
    public Slider slider;
    public TextMeshProUGUI loadingText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);

    }
    public void LoadSceneAsync()
    {
        MenuManager.instance.StopBubbleAnimation();
        ResetLoadingGUI();
        loadingPanel.SetActive(true);   
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Gameplay");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            float prog = Mathf.Clamp01(asyncLoad.progress);
            slider.value = prog;
            loadingText.text = "Loading " + (prog * 100f).ToString("0") + "%";
            yield return null;
        }

        slider.value = 1f;
        loadingText.text = "Loading 100%";

        ResetLoadingGUI();
    }

    public void ResetLoadingGUI()
    {
        loadingPanel.SetActive(false);   
        slider.value = 0f;
        loadingText.text = "Loading 0%";
    }

}
