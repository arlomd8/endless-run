using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderboardData : MonoBehaviour
{
    public int score;
    public string userName;

    public int rank;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI numberText;

    public Image medalPlaceholder;
    public Sprite[] medals;
    
    public void SetData(int _rank, string _name, int _coin)
    {
        userName = _name;
        score = _coin;  
        rank = _rank;


        numberText.text = rank.ToString();
        scoreText.text = score.ToString();
        nameText.text = userName;

        if(rank > 3)
        {
            medalPlaceholder.sprite = medals[0]; //
        }
        else
        {
            medalPlaceholder.sprite = medals[rank]; //
        }
    }
}
