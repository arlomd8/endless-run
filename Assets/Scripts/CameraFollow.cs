using System.Collections;
using System.Collections.Generic;
using Unity.Properties;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public float smoothTime;
    public Vector3 offset;

    public Vector3 velocity;

    // private void FixedUpdate() 
    // {
    //     Vector3 targetPosition = player.position + offset;
    //     transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    // }

    private void Update() 
    {
        Vector3 targetPosition = player.position + offset;
        transform.position = targetPosition;
    }
}
