using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;
using static System.Net.Mime.MediaTypeNames;

public class QRCodeGenerator : MonoBehaviour
{

    [SerializeField] private string textURL;

    [SerializeField] private RawImage rawImage;
    [SerializeField] private Texture2D encodedTexture;
    private void Start()
    {
        encodedTexture = new Texture2D(256, 256);
    }

    public void SetText(string _textUrl)
    {
        textURL = _textUrl;
    }

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        BarcodeWriter writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    public void GenerateQR(string text)
    {
        EncodeTextToQRCode(text);
    }

    private void EncodeTextToQRCode(string text)
    {
        if(text == null || text == "")
        {
            return;
        }
        var encoded = new Texture2D(256, 256);

        Color32[] convertPixelToTexture = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(convertPixelToTexture);
        encoded.Apply();

        rawImage.texture = encoded;

    }

    public Texture2D GenerateQRTexture(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

   
}
