using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public static VideoManager instance;
    public VideoPlayer videoPlayer;

    private void Awake() 
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    public void Start()
    {
        StartDefaultVideo();
    }
    public void StartDefaultVideo()
    {
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = Application.streamingAssetsPath + "/" + "Video.mp4";
        videoPlayer.Play();
    }

    public void StopDefaultVideo()
    {
        videoPlayer.Stop();
    }
}
