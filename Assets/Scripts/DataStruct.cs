using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Player
{
    public string name;
    public int score;
}

[Serializable]
public class LeaderboardPage
{
    public int total;
    public int totalPages;
    public int currentPage;
}

[Serializable]
public class Leaderboard
{
    public LeaderboardPage meta;
    public Player[] data;
    public string additional;
}

[Serializable]
public class PlayerList
{
    public List<Player> players;
}
