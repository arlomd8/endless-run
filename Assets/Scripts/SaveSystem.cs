using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Text;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;
using UnityEditor;

public class SaveSystem : MonoBehaviour
{   
    public static SaveSystem instance;
    public string FILE_EXTENTION = ".tphlbdata";
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }
    
    public object LoadData(string fileName)
    {
        //Debug.Log("LOAD DATA");
        string dataPath = Application.streamingAssetsPath + "/" + fileName + FILE_EXTENTION;
        if (File.Exists(dataPath))
        {
            try
            {
                // Create a BinaryFormatter.
                BinaryFormatter formatter = new BinaryFormatter();

                // Open the file at the specified path.
                using (FileStream fileStream = File.Open(dataPath, FileMode.Open))
                {
                    // Deserialize and return the object.
                    //Debug.Log("Data loaded successfully.");
                    return formatter.Deserialize(fileStream);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Error loading data: " + e.Message);
                return null;
            }
        }

        // Return null if the file doesn't exist or there's an error.
        return null;
    }

    public void SaveData(object data, string fileName)
    {
        //Debug.Log("SAVE DATA");
        string dataPath = Application.streamingAssetsPath + "/" + fileName + FILE_EXTENTION;
        try
        {
            // Create a BinaryFormatter.
            BinaryFormatter formatter = new BinaryFormatter();

            // Create or open the file at the specified path.
            using (FileStream fileStream = File.Open(dataPath, FileMode.Create))
            {
                // Serialize and write the object to the file.
                formatter.Serialize(fileStream, data);
            }

            //Debug.Log("Data saved successfully.");
        }
        catch (Exception e)
        {
            Debug.LogError("Error saving data: " + e.Message);
        }
    }


    public void ToCSV(PlayerList playerList, UnityAction<string> csvData)
    {
        var sb = new StringBuilder("Rank,Name,Score");
        for(int i = 0; i < playerList.players.Count; i++)
        {
            sb.Append('\n').Append((i + 1).ToString())
                .Append(',').Append(playerList.players[i].name)
                .Append(',').Append(playerList.players[i].score.ToString());
        }

        csvData(sb.ToString());
    }

    public void ExtractCSV(string csvData)
    {
        // Use the CSV generation from before
        var content = csvData;

        // The target file path e.g.

        var folder = Application.streamingAssetsPath;

        if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

        //var folder = Application.persistentDataPath;


        var filePath = Path.Combine(folder, "Leaderboard.csv");

        using (var writer = new StreamWriter(filePath, false))
        {
            writer.Write(content);
        }

        // Or just
        //File.WriteAllText(content);

        FindObjectOfType<AudioManager>().Play("csv");
        //Debug.Log($"CSV file written to \"{filePath}\"");

#if UNITY_EDITOR
        AssetDatabase.Refresh();
#endif
    }

}
