using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class LeaderboardManager : MonoBehaviour
{
    
    public GameObject prefabPlayer;
    public Transform spawnParent;
    public List<GameObject> PrefabList;

    public PlayerList playerList = new();

    private void Start() 
    {
        object pL = SaveSystem.instance.LoadData("Leaderboard"); 
        if(pL == null) return;

        playerList = (PlayerList) pL;
        UserManager.instance.playerList = playerList;

        for (int i = 0; i < playerList.players.Count; i++)
            {
                if(i >= 10)
                {
                    break;
                }
                GameObject playerLeaderboard = Instantiate(prefabPlayer, spawnParent, worldPositionStays: false);

                int rank = i + 1;
                string name = $"{playerList.players[i].name}";
                int score = playerList.players[i].score;

                playerLeaderboard.GetComponent<LeaderboardData>().SetData(rank, name, score);

                //PlayerList.Add(playerList.players[i]);
                PrefabList.Add(playerLeaderboard);
            }
        
    }


    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                SaveSystem.instance.ToCSV(playerList, (csvData) =>
                {
                    if (csvData != null || csvData != "")
                    {
                        SaveSystem.instance.ExtractCSV(csvData);
                    }
                });
            }
        }
    }



}
