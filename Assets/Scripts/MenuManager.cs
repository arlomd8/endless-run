using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG;
using DG.Tweening;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    public Image bubble1;
    public Image bubble2;
    public Image bubble3;

   private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        
    }
    public void Start(){

        if(UserManager.instance == null){
            return;
        }

        UserManager.instance.ResetUser();
        AnimateBubbles();
    }

    public void AnimateBubbles(){
        bubble1.GetComponent<RectTransform>()
            .DOAnchorPosY(bubble1.rectTransform.localPosition.y + 60, 2f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

        bubble2.GetComponent<RectTransform>()
            .DOAnchorPosY(bubble2.rectTransform.localPosition.y - 40, 2f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

        bubble3.GetComponent<RectTransform>()
            .DOAnchorPosY(bubble3.rectTransform.localPosition.y + 20, 2f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

    }

    public void StopBubbleAnimation()
    {
        bubble1.DOKill();
        bubble2.DOKill();
        bubble3.DOKill();
        
    }

}
