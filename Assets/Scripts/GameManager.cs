using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Windows.Kinect;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool usingKinect;
    public GameObject playerObject;
    public Animator playerAnim;

    [Header("Spawn Section")]
    public GameObject sectionPrefab;
    public GameObject sectionTwoPrefab;
    public Transform nextSpawnPoint;
    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public UnityAction OnRoadSpawn;
    public UnityAction OnRoadTwoSpawn;


    [Header("Gameplay")]
    public bool isPlaying;
    public int botolCounter;

    [Header("Score")]
    public int currentScore;
    public int scorePerItem;
    public int uspCounter;
   

    [Header("Gametimer")]
    public float maxTime;
    public float currentTime;
    public UnityAction<float, float> OnTimeTicking;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        isPlaying = false;
        currentTime = maxTime;
        currentScore = 0;

        
        
        //FindObjectOfType<AudioManager>().RandomBGM();
        
        UIManager.instance.SetScoreGUI(currentScore);
        UIManager.instance.SetTimerGUI(currentTime, maxTime);
        
        StartCoroutine(GameStart(3));
    }

    public IEnumerator GameStart(int timer)
    {
        if(timer > 0)
        {
            FindObjectOfType<AudioManager>().Play("C1");
        }
        else
        {

            FindObjectOfType<AudioManager>().Play("C2");
        }

        yield return new WaitForSeconds(1);
        
        timer -= 1; 
        UIManager.instance.SetGameStart(timer);

        if(timer < 0)
        {
            isPlaying = true;
            playerAnim.SetBool("isRunning", true);
            UIManager.instance.HideGameStart();
            StartCoroutine(StartTimer());
            yield return null;
        }
        else
        {
            StartCoroutine(GameStart(timer));
        }
    }

    public IEnumerator GameOver(int timer)
    {
        UIManager.instance.ShowGameOverPanel();

        yield return new WaitForSeconds(1);

        timer -= 1; 
        UIManager.instance.SetGameOver(timer);

        if(timer < 0)
        {
            UIManager.instance.ChangeScene("Result");
            yield return null;
        }
        else
        {
            StartCoroutine(GameOver(timer));
        }
    }
    public void SpawnSection()
    {
        //GameObject temp = Instantiate(sectionPrefab, nextSpawnPoint.transform.position, Quaternion.identity);
        //nextSpawnPoint = temp.transform.GetChild(0);

        if (!sectionTwoPrefab.activeInHierarchy)
        {
            Vector3 currentPos = new(0, 0, spawnPoint1.position.z);

            sectionTwoPrefab.transform.position = currentPos;
            sectionTwoPrefab.SetActive(true);
            OnRoadTwoSpawn?.Invoke();
        }

        else if (!sectionPrefab.activeInHierarchy)
        {
            Vector3 currentPos = new(0, 0, spawnPoint2.position.z);

            sectionPrefab.transform.position = currentPos;
            sectionPrefab.SetActive(true);
            OnRoadSpawn?.Invoke();
        }

    }

    public void PlayerTakeDamage(int currentHealth, int maxHealth)
    {
        currentScore -= 0;
        UIManager.instance.SetHealthGUI(currentHealth, maxHealth);
        UIManager.instance.ShowRedAlert();

        if (currentScore <= 0)
        {
            currentScore = 0;
        }

        UserManager.instance.score = currentScore;
        UIManager.instance.SetScoreGUI(currentScore);

        if (currentHealth <= 0)
        {
            isPlaying = false;
            playerAnim.SetBool("isRunning", false);
            playerObject.SetActive(false);
            UserManager.instance.score = currentScore;
            StartCoroutine(GameOver(3));
        }
    }

    public void PlayerTakeItem(int s)
    {
        currentScore += s;
        UIManager.instance.ShowGreenAlert();
        if(currentScore % uspCounter == 0)
        {
            //UIManager.instance.ShowUSPText();
        }

        UserManager.instance.score = currentScore;
        UIManager.instance.SetScoreGUI(currentScore);
    }

    public IEnumerator StartTimer()
    {
        yield return null;
        if (currentTime > 0 && isPlaying)
        {
            currentTime -= Time.deltaTime;
        }
        else
        {
            currentTime = 0;
        }

        OnTimeTicking?.Invoke(currentTime, maxTime);

        if (currentTime > 0 && isPlaying)
        {
            StartCoroutine(StartTimer());
        }
        else
        {
            isPlaying = false;
            playerAnim.SetBool("isRunning", false);
            StopCoroutine(nameof(StartTimer));
            playerObject.SetActive(false);
            UserManager.instance.score = currentScore;
            StartCoroutine(GameOver(3));
        }
    }

    public void ChangeKinectMode()
    {
        if(usingKinect)
        {
            usingKinect = false;
        }
        else
        {
            usingKinect = true;
        }
    }
    




}
