using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceCommand : MonoBehaviour
{
    private Dictionary<string, Action> keywordActions = new Dictionary<string, Action>();

    private KeywordRecognizer keywordRecognizer;
    private DictationRecognizer dictationRecognizer;

    private void Start()
    {

        keywordActions.Add("poochoock", RaiseBottle);

        keywordRecognizer = new KeywordRecognizer(keywordActions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += OnKeywordRecognized;
        keywordRecognizer.Start();
        //startDictationRequest();
    }


    public void OnKeywordRecognized(PhraseRecognizedEventArgs args)
    {
        print("Keyword: " + args.text);
        keywordActions[args.text].Invoke();
    }

    public void RaiseBottle()
    {
        print("poochoock");
    }


    public void startDictationRequest()
    {

        // THIS IS REALLY IMPORTANT!!
        // If you are using the MRTK and have phrase recognition running, then you can't use both at once,
        // so you need to turn it off with the following line
        if (PhraseRecognitionSystem.Status == SpeechSystemStatus.Running)
        {
            PhraseRecognitionSystem.Shutdown();
        }

        StartDictationEngine();

    }

    // CALL THIS FUNCTION WHEN YOU WANT TO STOP LISTENING TO DICTATION
    public void stopDictationRequest()
    {

        // THIS IS REALLY IMPORTANT!!
        // If you are using the MRTK and turned off phrase recognition to enable dictation
        // you need to turn it back ON again with the following line
        if (PhraseRecognitionSystem.Status != SpeechSystemStatus.Running)
        {
            PhraseRecognitionSystem.Restart();
        }

        CloseDictationEngine();
    }

    private void DictationRecognizer_OnDictationHypothesis(string text)
    {

        // So in here is the hypothesised text. It's quick, like as fast as it's spoken you'll get the string passed in here
        // but it's slightly less accurate. So test what you get and see if it's close enough,
        // otherwise do your checking for your phrases in the OnDictationResult method

        if (text.Contains("poochoock"))
        {
            RaiseBottle();
        }
    }

    private void DictationRecognizer_OnDictationComplete(DictationCompletionCause completionCause)
    {
        switch (completionCause)
        {
            case DictationCompletionCause.TimeoutExceeded:
            case DictationCompletionCause.PauseLimitExceeded:
            case DictationCompletionCause.Canceled:
            case DictationCompletionCause.Complete:
                // Restart required
                CloseDictationEngine();
                StartDictationEngine();
                break;
            case DictationCompletionCause.UnknownError:
            case DictationCompletionCause.AudioQualityFailure:
            case DictationCompletionCause.MicrophoneUnavailable:
            case DictationCompletionCause.NetworkFailure:
                // Error
                CloseDictationEngine();
                break;
        }
    }

    private void DictationRecognizer_OnDictationResult(string text, ConfidenceLevel confidence)
    {

        // Debug.Log("Dictation result: " + text);
        if (text.Contains("poochoock"))
        {
            RaiseBottle();
            //messageText.text = "RESULT CONTAINS IN : " + text;
        }
        else
        {
            // Debug.Log("RESULT does not CONTAIN IN!!!");
        }
    }
    private void DictationRecognizer_OnDictationError(string error, int hresult)
    {
        //messageText.text = "Dictation error : " + error;
        Debug.Log("Dictation error: " + error);
    }
    private void OnApplicationQuit()
    {
        CloseDictationEngine();
    }
    private void StartDictationEngine()
    {
        Debug.Log("StartDictationEngine");
        dictationRecognizer = new DictationRecognizer();
        dictationRecognizer.DictationHypothesis += DictationRecognizer_OnDictationHypothesis;
        dictationRecognizer.DictationResult += DictationRecognizer_OnDictationResult;
        dictationRecognizer.DictationComplete += DictationRecognizer_OnDictationComplete;
        dictationRecognizer.DictationError += DictationRecognizer_OnDictationError;
        dictationRecognizer.Start();
    }
    public void CloseDictationEngine()
    {
        if (dictationRecognizer != null)
        {
            dictationRecognizer.DictationHypothesis -= DictationRecognizer_OnDictationHypothesis;
            dictationRecognizer.DictationComplete -= DictationRecognizer_OnDictationComplete;
            dictationRecognizer.DictationResult -= DictationRecognizer_OnDictationResult;
            dictationRecognizer.DictationError -= DictationRecognizer_OnDictationError;
            if (dictationRecognizer.Status == SpeechSystemStatus.Running)
            {
                dictationRecognizer.Stop();
            }
            dictationRecognizer.Dispose();
        }
    }
}
