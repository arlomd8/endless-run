using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRandom : MonoBehaviour
{
    public GameObject[] ulat;

    public void RandomizeEnemyAsset()
    {
        int r = Random.Range(0,ulat.Length);

        foreach (GameObject u in ulat)
        {
            u.SetActive(false);
        }

        ulat[r].SetActive(true);
    }
}
