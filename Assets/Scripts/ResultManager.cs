using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine.SocialPlatforms.Impl;
using System.Collections.Concurrent;

public class ResultManager : MonoBehaviour
{
    [Header("General Bubbles")]
     public Image bubble1;
    public Image bubble2;
    public Image bubble3;

    [Header("User Section")]
    public string userName;
    public int finalScore;
    public int cardIndex;

    [Header("Result Section")]
    public TextMeshProUGUI finalScoreText;
    public TMP_InputField inputField;
    public Button resultButton;

    [Header("Card Section")]
    public TextMeshProUGUI cardNameText;
    public TextMeshProUGUI cardScoreText;

    public Image cardPlaceHolder;
    public Sprite[] cardTier;

    //Need to improve
    [Header("QR Section")]
    public QRCodeGenerator qrCodeGenerator;
    public const string LINK = "https://g.minigim.fun/teh-pucuk-harum-result/index.html";
    

    private void Start()
    {   
        //FindObjectOfType<AudioManager>().RandomBGM();

        if (UserManager.instance == null)
            return;

        finalScore = UserManager.instance.score;
        resultButton.interactable = false;
        SetFinalScore(finalScore);
        AnimateBubbles();
    }

    public void SetFinalScore(int _score)
    {
        finalScoreText.text = _score.ToString();
        
        //Checking Tier
        if(_score >= 0 && _score <= 100)
        {
            cardIndex = 0;
        }
        else if (_score >= 101 && _score <= 250)
        {
            cardIndex = 1;
        }
        else if (_score >= 251 && _score <= 500)
        {
            cardIndex = 2;
        }
        else if (_score >= 501)
        {
            cardIndex = 3;
        }
    }

    //Call on button
    public void SetCard()
    {
        userName = inputField.text;

        cardNameText.text = userName;
        cardScoreText.text = finalScore.ToString();

        cardPlaceHolder.sprite = cardTier[cardIndex];

        SubmitLeaderboard();

        UserManager.instance.username = userName;
        UserManager.instance.score = finalScore;

        SetBarcode(userName, finalScore);
    }

    public void SetBarcode(string pname, int pscore)
    {
        string address = $"{LINK}?pName={pname}&pScore={pscore}";

        qrCodeGenerator.GenerateQR(address);
        //FindObjectOfType<AudioManager>().Play("Result");
    }

    public void CheckInputField()
    {
        if(inputField.text.Length > 2)
        {
            resultButton.interactable = true;
        }
        else
        {
            resultButton.interactable = false;
        }
        userName = inputField.text;
        UserManager.instance.username = userName;
    }

    public void AnimateBubbles(){
        bubble1.GetComponent<RectTransform>()
            .DOAnchorPosY(bubble1.rectTransform.localPosition.y + 60, 2f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

        bubble2.GetComponent<RectTransform>()
            .DOAnchorPosY(bubble2.rectTransform.localPosition.y - 40, 2f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

        bubble3.GetComponent<RectTransform>()
            .DOAnchorPosY(bubble3.rectTransform.localPosition.y + 20, 2f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

    }

     public void StopBubbleAnimation()
    {
        bubble1.DOKill();
        bubble2.DOKill();
        bubble3.DOKill();
        
    }

    public void SubmitLeaderboard()
    {
        Player newPlayer = new() { name = userName, score = finalScore};

        if(UserManager.instance != null)
        {
            UserManager.instance.SubmitToLeaderBoard(newPlayer);
        }        
    }

    
}
