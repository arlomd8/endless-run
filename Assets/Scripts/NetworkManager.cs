 using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using static System.Net.WebRequestMethods;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    public const string TOKEN = "01f172e3-f0c7-4789-aded-cc90653b4a23:D6Rdz03NIzgLVefjrUl221ZW28rWHmdc";
    public string BASE_URL = "https://cms.dexa-game.com";


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }
 public void GetLeaderboard(int _page, UnityAction<string> callback)
    {
        StartCoroutine(RequestLeaderboard(_page,callback));
    }

    private IEnumerator RequestLeaderboard(int _page, UnityAction<string> callback)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(BASE_URL + $"/v1/ogb/leaderboard?page={_page}&size=6");
        webRequest.SetRequestHeader("Authorization", TOKEN);

        using (webRequest)
        {
            yield return webRequest.SendWebRequest();

            
            

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.LogError("Error: " + webRequest.error);
                callback(webRequest.error);
            }
            else if (webRequest.isDone)
            {
                var json = webRequest.downloadHandler.text;
                callback(json);
            }
        }

    }

}
