using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CommonScript : MonoBehaviour
{
   
    private void Start() {
        Application.targetFrameRate = -1;
    }
    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MuteAudio()
    {
        if (!FindObjectOfType<AudioManager>().isMute)
        {
            FindObjectOfType<AudioManager>().Mute();
        }
        else
        {
            FindObjectOfType<AudioManager>().UnMute();
        }

    }
    public void UnMuteAudio()
    {
        FindObjectOfType<AudioManager>().UnMute();
    }

    public void ButtonClick()
    {
        FindObjectOfType<AudioManager>().Play("Click");
    }


    public void EnableAudio(Image img)
    {
        if (!FindObjectOfType<AudioManager>().isMute)
        {
            MuteAudio();
            img.color = Color.gray;
        }
        else
        {
            UnMuteAudio();
            img.color = Color.white;
        }
    }

    public void LoadGameplay()
    {
        if(LoadingManager.instance == null)
            return;

        LoadingManager.instance.LoadSceneAsync();
    }
    
}
