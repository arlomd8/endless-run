﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Joint = Windows.Kinect.Joint;
using Windows.Kinect;
using UnityEngine.Rendering.Universal;

public class BodySourceView : MonoBehaviour 
{
    public Material BoneMaterial;
    public BodySourceManager BodySourceManager;
    public PlayerHandler PlayerHandler;
    public GameObject jointObject;
    
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
   

    private List<JointType> JointTypes = new()
    {
        JointType.SpineBase
    };
    
    void Update () 
    {
        #region GetData
        //Checking Data
        Body[] data = BodySourceManager.GetData();
        if (data == null)
        {
            GameManager.instance.usingKinect = false;
            return;
        }
        else
        {
            GameManager.instance.usingKinect = true;
        }
        
        List<ulong> trackedIds = new List<ulong>();
        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
            }
                
            if(body.IsTracked)
            {
                trackedIds.Add (body.TrackingId);
            }
        }

        #endregion

        #region Delete Tracked
        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }
        #endregion

        #region Create Tracked Data
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }
            
            if(body.IsTracked)
            {
                if(!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }
                
                RefreshBodyObject(body, _Bodies[body.TrackingId]);
            }
        }
        #endregion
    }

    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);
        
        foreach (JointType jt in JointTypes)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            jointObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }
        
        return body;
    }
    
    private void RefreshBodyObject(Body body, GameObject bodyObject)
    {

        foreach (JointType jt in JointTypes)
        {
            Joint sourceJoint = body.Joints[jt];
            //Joint? targetJoint = null;
            Vector3 targetJoint = GetVector3FromJoint(sourceJoint);
            targetJoint.z = 0;
            
           
            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            //print(jointObj.localPosition.x);


            PlayerHandler.UpdatePositionByKinect(jointObj);
            
            if(jointObj.localPosition.x > -1.8f && jointObj.localPosition.x < 1.8f)
            {
                
            }
            
        }
    }
    
    private static Vector3 GetVector3FromJoint(Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}
