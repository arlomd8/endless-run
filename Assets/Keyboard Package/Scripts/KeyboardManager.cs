using UnityEngine;
using TMPro;

public class KeyboardManager : MonoBehaviour
{
    public static KeyboardManager Instance;

    [SerializeField] TMP_InputField inputField;
    public GameObject keyboard;

    private void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        inputField.text = "";
    }

    public void DeleteLetter()
    {
        if(inputField.text.Length != 0) {
            inputField.text = inputField.text.Remove(inputField.text.Length - 1, 1);
        }
    }

    public void AddLetter(string letter)
    {
        inputField.text = inputField.text + letter;
    }

    public void SubmitWord()
    {
        keyboard.SetActive(false);
        //printBox.text = inputField.text;
        //inputField.text = "";
        // Debug.Log("Text submitted successfully!");
    }
}
